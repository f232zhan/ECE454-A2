import org.apache.spark.{SparkContext, SparkConf}

// please don't change the object name
object Task1 {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Task 1")
    val sc = new SparkContext(conf)

    val textFile = sc.textFile(args(0))

    val output = textFile.map { line =>
      //split each line of the text 
      val parts = line.split(",")
      //movie title is the first item of a row 
      val movieTitle = parts(0)
      //collection of items except the first 
      val ratings = parts.tail

      // Finds the highest rating of each row 
      val highestRating = ratings.filter(_.nonEmpty).map(_.toInt).max
      //if rating is the highest, its index is collected 
      val highestRatingColumns = ratings.zipWithIndex.collect {
        case (rating, index) if rating.nonEmpty && rating.toInt == highestRating => (index + 1).toString
      }
      
      //the final string with movie title and index of highest ratings attached 
      s"$movieTitle,${highestRatingColumns.mkString(",")}"
    }
    
    output.saveAsTextFile(args(1))
  }
}
