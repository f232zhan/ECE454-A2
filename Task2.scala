import org.apache.spark.{SparkContext, SparkConf}

// please don't change the object name
object Task2 {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Task 2")
    val sc = new SparkContext(conf)

    val textFile = sc.textFile(args(0))

    val totalRatings = textFile.map { line =>
      line.split(",").tail.count(_.nonEmpty)  
      // Sum all counts from each line 
    }.reduce(_ + _)  

    // converts totalRatings to an RDD for output
    val output = sc.parallelize(Array(totalRatings))
    //saveAsTextFile only works with RDD collection 
    output.saveAsTextFile(args(1))
  }
}
