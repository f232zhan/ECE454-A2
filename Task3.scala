import org.apache.spark.{SparkContext, SparkConf}

// please don't change the object name
object Task3 {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Task 3")
    val sc = new SparkContext(conf)

    val textFile = sc.textFile(args(0))

    val output = textFile
    //to create single list of tuples rather than multiple lists of tuples with each list being each row 
      .flatMap { line =>  
        val ratings = line.split(",").tail  
        //each rating zipped with its index
        //case extract the pattern (rating, index) and maps these values to (index + 1, rating)
        ratings.zipWithIndex.map { case (rating, index) => (index + 1, if (rating.nonEmpty) 1 else 0) }
      }
      //sum up ratings by key (user) and sort 
      .reduceByKey(_ + _)  
      .sortByKey()  
      .map(tuple => tuple._1 + "," + tuple._2)
    
    output.saveAsTextFile(args(1))
  }
}
