import org.apache.spark.{SparkContext, SparkConf}

// please don't change the object name
object Task4 {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Task 4")
    val sc = new SparkContext(conf)

    val textFile = sc.textFile(args(0))

    //each rating contains movie name along with its set of user ratings
    val ratings = textFile.map { line =>
      val parts = line.split(',')
      val movie = parts(0)
      val userRatings = parts.tail.map(rating => if (rating.isEmpty) None else Some(rating.toInt))
      (movie, userRatings)
    }.cache()  

    //forms all possible pairs of movies and ratings with each pair sorted by movie name 
    val moviePairs = ratings.cartesian(ratings)
      .filter { case ((movie1, _), (movie2, _)) => movie1 < movie2 }  

    //compare similarities between pairs of movies 
    val output = moviePairs.map { case ((movie1, ratings1), (movie2, ratings2)) =>
      //compare ratings in the same index position, count # of user that gave same ratings for both movies, 
      //and both of the ratings are not none 
      val commonRatings = ratings1.zip(ratings2)
        .count { case (rating1, rating2) => rating1 == rating2 && rating1.isDefined }
      //output format 
      s"$movie1,$movie2,$commonRatings"
    }
    
    output.saveAsTextFile(args(1))
  }
}
