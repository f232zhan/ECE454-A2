export JAVA_HOME := /usr/lib/jvm/java-8-openjdk-amd64/
export SCALA_HOME := /usr
export HADOOP_HOME := /opt/hadoop-latest/hadoop
export HADOOP_CONF_DIR := $(HADOOP_HOME)/etc/hadoop/
export SPARK_HOME := /opt/spark-latest/
MAIN_SPARK_JAR := $(shell ls $(SPARK_HOME)/jars/spark-core*.jar)
export CLASSPATH := .:$(MAIN_SPARK_JAR)

# Change this for different modes
export MODE := CLUSTER
INPUT_FILE := in5.txt

INPUT := /a2_inputs/${INPUT_FILE}
OUTPUT := /tmp/user/${USER}

# Default target
.PHONY: all
all: SparkWC

# Scala file targets
SparkWC: SparkWC.scala
	$(MAKE) run SCALA_FILE=$<

Task1: Task1.scala
	$(MAKE) run SCALA_FILE=$<

Task2: Task2.scala
	$(MAKE) run SCALA_FILE=$<

Task3: Task3.scala
	$(MAKE) run SCALA_FILE=$<

Task4: Task4.scala
	$(MAKE) run SCALA_FILE=$<

.PHONY: run
run: check-environment clean compile jar execute

check-environment:
	@hostname | grep ecehadoop
	@if [ $$? -eq 1 ]; then \
		echo "This script must be run on ecehadoop :("; \
		exit -1; \
	fi

clean:
	@echo "--- Deleting"
	-rm *.jar
	-rm *.class

compile:
	@echo "--- Compiling"
	$(SCALA_HOME)/bin/scalac -J-Xmx1g $(SCALA_FILE)
	@if [ $$? -ne 0 ]; then \
		exit 1; \
	fi

jar:
	@echo "--- Jarring"
	$(JAVA_HOME)/bin/jar -cf $(SCALA_FILE:.scala=.jar) $(SCALA_FILE:.scala=*.class)

execute:
	@echo "--- Running"
	$(HADOOP_HOME)/bin/hdfs dfs -rm -R $(OUTPUT)
	@if [ "$(MODE)" = "STANDALONE" ]; then \
		time $(SPARK_HOME)/bin/spark-submit --master local[2] --class $(SCALA_FILE:.scala=) --driver-memory 1g --executor-memory 1g $(SCALA_FILE:.scala=.jar) $(INPUT) $(OUTPUT); \
	else \
		time $(SPARK_HOME)/bin/spark-submit --master yarn --class $(SCALA_FILE:.scala=) --driver-memory 4g --executor-memory 4g $(SCALA_FILE:.scala=.jar) $(INPUT) $(OUTPUT); \
	fi
	export HADOOP_ROOT_LOGGER="WARN"
	$(HADOOP_HOME)/bin/hdfs dfs -ls $(OUTPUT)
	$(HADOOP_HOME)/bin/hdfs dfs -cat $(OUTPUT)/* > out.txt
	sort out.txt > sorted.txt
	rm out.txt
	mv sorted.txt result.txt
